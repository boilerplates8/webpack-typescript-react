
import {IAppState} from './IAppState';

// Aqui el objeto de tipo IAppState con la configuracion basica para arrancar la aplicacion

const state: IAppState = {

    person: {
        name: '',
        lastName: '',
        age: 0
    }

};


export const initialState = {...state};

// TODO: Investigar como meter eso del NODE_ENV en la aplicacion.
// Cuando este configurado eliminar el initialState de encima y exportar este

// export const initialState = process.env.NODE_ENV === 'development' ? {
//     ...state,
//     person: {
//         ...state.person,
//         name: 'Gerard',
//         lastName: 'Albiol',
//         age: 26
//     }
// } : state;