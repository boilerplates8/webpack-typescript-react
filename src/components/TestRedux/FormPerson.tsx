
import * as React from 'react';
import {FunctionComponent, useState, ChangeEvent, FormEvent} from 'react';
import {useDispatch} from 'react-redux';

import {Dispatch} from '../../store/types/Dispatch';
import {updatePerson} from '../../store/actions/personActions';
import { IPersonState } from '../../store/IAppState';

export const FormPerson: FunctionComponent = () => {

    const initialState = {name: '', lastName: '', age: 0}
    const [personForm, setPersonForm] = useState(initialState);

    const dispatch: Dispatch = useDispatch();
    const updatePersonDispatch = (state: IPersonState) => dispatch(updatePerson(state));


    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {

        setPersonForm({
            ...personForm,
            [event.target.name]: event.target.value
        });
    }

    const onFormSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        event.stopPropagation();

        updatePersonDispatch(personForm);
        setPersonForm(initialState);
    }

    const {name, lastName, age} = personForm;

    return (
        <div>
            Redux (Dispatch changes)

            <form onSubmit={onFormSubmit} >
                <input type="text" name="name" placeholder="name" value={name} onChange={onInputChange} />
                <input type="text" name="lastName" placeholder="lastName" value={lastName} onChange={onInputChange} />
                <input type="number" name="age" placeholder="age" value={age} onChange={onInputChange} />

                <button type="submit" >
                    Update
                </button>
            </form>
        </div>
    );
}