
import * as React from 'react';
import {FunctionComponent} from 'react';

import {Container, Row, Col} from '../../../__lib__/react-components/layout';

import {JustImg} from '../../JustImg';
import {TestRedux, FormPerson} from '../../TestRedux';


const styles = require('./HomePage.scss');

export const HomePage: FunctionComponent = () => {

    return (
        <Container>

            <h1 className={styles.title} >
                Foo
            </h1>

            <div className={styles.subTitle} >
                App file
            </div>

            <TestRedux />
            <FormPerson />

            <JustImg image="/images/foo.jpg" />

            <Row>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 1
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 2
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 3
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 4
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 5
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 6
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 7
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 8
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 9
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 10
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 11
                </Col>
                <Col xs={12} sm={6} md={4} lg={2} >
                    Col 12
                </Col>
            </Row>

        </Container>
    );
}
