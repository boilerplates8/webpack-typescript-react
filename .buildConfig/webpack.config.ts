
import {Configuration} from 'webpack';
import path from 'path';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';


const config: Configuration = {

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '..', 'public'),
        publicPath: '/'
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Boilerplate - Typescript React',
            template: path.resolve(__dirname, '..', 'src', 'index.ejs')
        }),
        new MiniCssExtractPlugin({
            filename: 'style.css'
        }),
        new FaviconsWebpackPlugin(path.resolve(__dirname, '..', 'public', 'favicon.ico'))
    ]
};


export default config;
