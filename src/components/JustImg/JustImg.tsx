
import * as React from 'react';
import {FC} from 'react';


const styles = require('./JustImg.scss');

interface JustImgProps {
    image: string;
}

export const JustImg: FC<JustImgProps> = (props) => {

    const {image} = props;

    return (
        <div className={styles.imgWrapper} >
            <img src={image} />
        </div>
    );
}
