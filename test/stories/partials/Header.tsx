
import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {INITIAL_VIEWPORTS} from '@storybook/addon-viewport';

import {Header} from '../../../src/components/partials';

import {ComponentWrapper} from '../utils';


storiesOf('partials/Header', module)
    .addDecorator(story => (
        <ComponentWrapper style={{display: 'block'}} >
            {story()}
        </ComponentWrapper>
    ))
    .add('Dark', () => (
        <Header theme="dark" />
    ), {viewport: {viewports: {...INITIAL_VIEWPORTS}, defaultViewport: 'ipad'}})
    .add('Light', () => (
        <Header theme="light" />
    ));