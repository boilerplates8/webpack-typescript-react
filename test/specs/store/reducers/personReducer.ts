
import {personReducer} from '../../../../src/store/reducers/personReducer';
import {initialState} from '../../../../src/store/initialState';
import {UPDATE_PERSON} from '../../../../src/store/types/personTypes';


describe('Reducers - personReducer', () => {

    const state = initialState.person;

    test('Should return initialState', () => {

        //Mocks
        const actionMock = {
            type: 'invalidType',
            payload: {}
        };

        // Simulation
        const res = personReducer(state, actionMock);

        // Assertion
        expect(res).toEqual(state);
    });

    test('Update person', () => {

        // Mocks
        const personMock = {
            name: 'Gerard',
            lastName: 'Albiol',
            age: 26
        };

        const actionMock = {
            type: UPDATE_PERSON,
            payload: personMock
        };

        // Simulation
        const res = personReducer(state, actionMock);

        // Assertion
        expect(res).toEqual(personMock);
        
    });

});