


export interface IAppState {

    person: IPersonState;

};

export interface IPersonState {

    name: string;

    lastName: string;

    age: number;

};
