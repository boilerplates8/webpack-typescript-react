
import {Reducer} from 'redux';

import {IPersonState} from '../IAppState';
import {initialState} from '../initialState';
import {UPDATE_PERSON} from '../types/personTypes';


export const personReducer: Reducer<IPersonState> = (state: IPersonState = initialState.person, {type, payload}) => {

    switch(type) {

        case UPDATE_PERSON:
            return {
                ...state,
                ...payload
            };

        default:
            return state;
    }
}