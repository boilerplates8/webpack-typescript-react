

export const routes = {
    home: '/'
};


/**
 * Function to replace the params inside a route.
 * 
 * @param   route   Route (string) with the params
 * @param   param   Object with a map to fill the params in route string
 */
// export const preparePath = (route, param) => {

//     let keyArray = Object.keys(param);
    
//     keyArray.forEach(key => {
//         route = route.replace(`:${key}`, param[key]);
//     });

//     return route;
// }