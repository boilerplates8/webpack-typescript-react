
import * as React from 'react';
import {FunctionComponent, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';

import {IAppState} from '../../store/IAppState';
import {initialState} from '../../store/initialState';


export const TestRedux: FunctionComponent = () => {

    const personSelector = useSelector((state: IAppState) => state.person);
    const [person, setPerson] = useState(personSelector);
    const {name, lastName, age} = person;

    useEffect(() => {

        if(personSelector !== initialState.person) {
            setPerson(personSelector);
        }

    }, [personSelector]);

    return (
        <div>
            Redux (Get from the store):

            <div>Name: {name}</div>
            <div>LastName: {lastName}</div>
            <div>Age: {age}</div>

        </div>
    );
}