# boilerplate-typescript-react

## About the boilerplate

This project is for single page applications using ReactJS framework
with Typescript. It includes a set of the most common utilities
and configurations to ease up testing and development. Between it's main features you can find:

- Typescript
- React
- Redux (Thunk)
- SCSS
- Storybook
- Jest

## Directory architecture

```
.
├── public
├── src
│   ├── components
│   ├── config - Some config params.
│   ├── model
│   ├── store
│   │   ├── actions
│   │   ├── reducers
│   │   └── typings - Helpful typings used for redux thunk
│   ├── styles - Basic and shared styles, variables and mixins.
│   ├── index.ejs - index.html template
│   └── index.tsx - React Dom render (development and production in diferent files, .dev.tsx and .prod.tsx)
└── test
    ├── specs - Tests for components, utils, redux, ...
    └── stories - Isolated Components
```

## Development

For development use the following scripts:

- `npm run dev:webpack` will launch a development environment at `http://localhost:3000`, served using 
`webpack-dev-server` with **hot module replacement** configured.

- `npm run dev:storybook` will launch a component development environment (**Storybook**)
 at `http://localhost:6006`.

## Testing
 
Inside the project you will find `test.ts` files inside "test/specs" folder.

For testing use the following script:

- `npm run test:jest` will run all test suites.

## Build and Deploy

To build the app execute this script `npm run build`. This will create everything needed to serve the app inside `public` folder.

**WIP:**
To build storybook execute this script `npm run build:storybook`. This will create a folder in the root with everything needed to serve storybook with any static file server.
