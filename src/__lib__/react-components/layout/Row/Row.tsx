
import * as React from 'react';
import {FunctionComponent} from 'react';


const styles = require('./Row.scss');

export const Row: FunctionComponent = (props) => {

    const {children} = props;

    return (
        <div className={styles.row} >

            {children}

        </div>
    );
}