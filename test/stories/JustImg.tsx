
import * as React from 'react';
import {storiesOf} from '@storybook/react';

import {JustImg} from '../../src/components/JustImg';

import {ComponentWrapper} from '../stories/utils';


storiesOf('partial/JustImg', module)
    .addDecorator(story => (
        <ComponentWrapper >
            {story()}
        </ComponentWrapper>
    ))
    .add('Default', () => (
        <div>
            yep
            <JustImg image="/images/foo.jpg" />
        </div>
    ))