
import * as React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

import {Button} from '../../../src/components/widgets';

import {ComponentWrapper} from '../utils/ComponentWrapper';


storiesOf('widgets/Button', module)
    .addDecorator(story => (
        <ComponentWrapper>
            {story()}
        </ComponentWrapper>
    ))
    .add('Default', () => (
        <Button text="Click me!" onClick={action('onClick')} />
    ));