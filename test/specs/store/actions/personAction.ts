
import createMockStore from 'redux-mock-store';
// import {MockStore} from 'redux-mock-store';
import thunk from 'redux-thunk';

import {updatePerson} from '../../../../src/store/actions/personActions';
import {Dispatch} from '../../../../src/store/types';
import {UPDATE_PERSON} from '../../../../src/store/types/personTypes';
import {initialState} from '../../../../src/store/initialState';
import {IAppState} from '../../../../src/store/IAppState';


const middlewares: any = [thunk];
// const mockStore = configureStore<IAppState, Dispatch>(middlewares);
const mockStore = createMockStore<IAppState, Dispatch>(middlewares);

describe('Action creators - personAction', () => {

    // TODO: Resolve this type conflict with the 'store.dispatch' in the test.
    // let store: MockStore;
    let store: any;

    beforeEach(() => {
        store = mockStore(initialState);
    });

    test('updatePerson', () => {

        // Mocks
        // TODO: Move to another __mock__ folder. Search about manual mocks and '__mock__' folder.
        const personMock = {
            name: 'Gerard',
            lastName: 'Albiol',
            age: 26
        };

        // Simulation
        store.dispatch(updatePerson(personMock)); // Conflict with store type (MockStore)
        const actions = store.getActions();

        // Assertions
        expect(actions[0]).toEqual({
            type: UPDATE_PERSON,
            payload: personMock
        });
    });
});