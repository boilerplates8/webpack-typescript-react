
import * as React from 'react';
import {Provider} from 'react-redux';
import {storiesOf} from '@storybook/react';

import {IAppState} from '../../src/store/IAppState';
import {TestRedux} from '../../src/components/TestRedux';

import {ComponentWrapper, createStoryStore} from './utils';


const state: IAppState = {

    person: {
        name: 'Gerard',
        lastName: 'Foo',
        age: 10
    }

};

const store = createStoryStore(state);

storiesOf('partial/TestRedux', module)
    .addDecorator(story => (
        <Provider store={store} >
            <ComponentWrapper>
                {story()}
            </ComponentWrapper>
        </Provider>
    ))
    .add('Default', () => (
        <TestRedux />
    ));