
import * as React from 'react';
import {Component, CSSProperties, ReactNode} from 'react';


interface ComponentWrapperProps {
    style?: CSSProperties;
    onMount?: Function;
    onDismount?: Function;
}

export class ComponentWrapper extends Component<ComponentWrapperProps> {

    public componentWillMount(): void {

        let {onMount} = this.props;

        if (onMount) {
            onMount();
        }
    }

    public componentWillUnmount(): void {

        let {onDismount} = this.props;

        if (onDismount) {
            onDismount();
        }
    }

    public render(): ReactNode {

        let {style, children} = this.props;

        return (
            <div style={{
                height: '100vh',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                ...style
            }} >

                {children}

            </div>
        );
    }

}