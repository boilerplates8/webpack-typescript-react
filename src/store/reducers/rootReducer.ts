
import {combineReducers, Reducer} from 'redux';

import {IAppState} from '../IAppState';
import {personReducer} from './personReducer';


export const rootReducer: Reducer<IAppState> = combineReducers({
    person: personReducer
});
